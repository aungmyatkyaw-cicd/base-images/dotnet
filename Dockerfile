FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine
RUN dotnet tool install --global dotnet-sonarscanner
RUN apk --update add openjdk17-jdk --no-cache \
    && wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
    && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.35-r1/glibc-2.35-r1.apk \
    && apk add glibc-2.35-r1.apk \
    && rm -f glibc-2.35-r1.apk
ENV JAVA_HOME="/usr/lib/jvm/java-17-openjdk"
ENV PATH=${JAVA_HOME}/bin:${PATH}:/root/.dotnet/tools
